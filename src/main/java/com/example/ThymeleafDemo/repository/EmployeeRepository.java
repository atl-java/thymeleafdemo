package com.example.ThymeleafDemo.repository;

import com.example.ThymeleafDemo.model.NewEmployee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<NewEmployee, Long> {
}
