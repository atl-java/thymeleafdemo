package com.example.ThymeleafDemo.service.impl;

import com.example.ThymeleafDemo.model.NewEmployee;
import com.example.ThymeleafDemo.repository.EmployeeRepository;
import com.example.ThymeleafDemo.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository empRepo;

    @Override
    public List<NewEmployee> getAllEmployee() {
        return empRepo.findAll();
    }

    @Override
    public void save(NewEmployee employee) {
        empRepo.save(employee);
    }

    @Override
    public NewEmployee getById(Long id) {
        Optional<NewEmployee> optional = empRepo.findById(id);
        NewEmployee employee = null;
        if (optional.isPresent())
            employee = optional.get();
        else
            throw new RuntimeException(
                    "Employee not found for id : " + id);
        return employee;
    }

    @Override
    public void deleteViaId(long id) {
        empRepo.deleteById(id);
    }

}
