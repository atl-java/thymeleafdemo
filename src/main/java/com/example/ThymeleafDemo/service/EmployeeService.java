package com.example.ThymeleafDemo.service;

import com.example.ThymeleafDemo.model.NewEmployee;

import java.util.List;

public interface EmployeeService {
    List<NewEmployee> getAllEmployee();
    void save(NewEmployee employee);
    NewEmployee getById(Long id);
    void deleteViaId(long id);
}
